### Arquitetura
O crawler possui uma arquitetura distribuída utilizando rpc e protocol buffers.
O client envia as urls para o servidor, que faz a requisição, parseia e retorna as urls presentes e informações do produto (caso seja uma pagina de produto).
    
### Execução
A partir do diretório raiz do projeto:
```sh
$ pip install -r requirements.txt
```

##### Iniciando o server:

```sh
$ python server/server.py
```

##### Iniciando o client:

```sh
$ python server/server.py
```

### Testes
A partir do diretório raiz:
```sh
$ python -m unittest server.parser_test
$ python -m unittest client.test
```

