
from unittest import TestCase
import parser
from proto import crawler_pb2
from mock import patch

class ParserTests(TestCase):
    def setUp(self):
        self.base_url = 'https://exemple.com.br/'
        self.match = '/p'
        self.price_selector = crawler_pb2.Selector(tag='price_tag',
                                                   class_name='price_class')
        self.product_selector = crawler_pb2.Selector(tag='product_tag',
                                                     class_name='product_class')
        self.product_url = 'https://exemple.com.br/product_name/p'
        self.site_url = 'https://exemple.com.br/something'

    def test_parser_url_fail(self):
        with patch('requests.get') as mock_request:
            url = self.site_url
            mock_request.return_value.content = "<html><tag>invalid html</tag></html>"
            response = parser.parse_url(url, self.base_url, self.match, self.price_selector, self.product_selector)

            expected = {'urls': [],
                        'product_url': '',
                        'price': '',
                        'title': ''}
            self.assertEqual(expected, response)

    def test_parser_url_product_success(self):
        with patch('requests.get') as mock_request:
            url = self.product_url
            mock_request.return_value.content = '<html><tag>valid html</tag>\
                                                <a href="{0}somepage/"></a>\
                                                <a href="https://anothersite.com.br"></a>\
                                                <{1} class="{2}">Product Price</{1}>\
                                                <{3} class={4}>Product Name</{3}>\
                                                </html>'.format(self.base_url,
                                                                self.price_selector.tag,
                                                                self.price_selector.class_name,
                                                                self.product_selector.tag,
                                                                self.product_selector.class_name)
            response = parser.parse_url(url, self.base_url, self.match, self.price_selector, self.product_selector)

            expected = {'urls': [self.base_url+'somepage/'],
                        'product_url': url,
                        'price': 'Product Price',
                        'title': 'Product Name'}
            self.assertEqual(expected, response)
