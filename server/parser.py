# -*- encoding:utf-8 -*-
import requests
from bs4 import BeautifulSoup
from fake_useragent import UserAgent


ua = UserAgent()


def parse_url(current_url, base_url, match, price_selector, title_selector):

    urls = []
    title = ''
    price = ''
    product_url = ''
    try:
        headers = {'User-Agent': ua.random}
        response = requests.get(current_url, headers=headers)
        html = BeautifulSoup(response.content, 'html.parser')
        for url in html.findAll('a', href=True):
            if is_site_page(url['href'], base_url):
                urls.append(url['href'])
        if is_product(current_url, match):
            price = get_price(html, price_selector)
            title = get_title(html, title_selector)
            product_url = current_url

    finally:
        return dict({'urls': urls,
                     'product_url': product_url,
                     'price': price,
                     'title': title})


def is_site_page(url, base_url):
    return url.startswith(base_url)


def is_product(url, match):
    return url.endswith(match)


def get_price(html, price_selector):
    try:
        price = html.find(price_selector.tag,
                          class_=price_selector.class_name).getText()
        return price
    except AttributeError:
        return ''


def get_title(html, title_selector):
    try:
        title = html.find(title_selector.tag,
                          class_=title_selector.class_name).getText()
        return title
    except AttributeError:
        return ''
