# -*- encoding:utf-8 -*-
import grpc
from proto import crawler_pb2_grpc
from proto import crawler_pb2
import time
from concurrent import futures
import parser


class CrawlerServicer(crawler_pb2_grpc.CrawlerServicer):
    def GetUrls(self, request, context):
        response = parser.parse_url(request.current_url,
                                    request.base_url,
                                    request.match,
                                    request.price_selector,
                                    request.title_selector)

        return crawler_pb2.List(url=response['urls'],
                                product_info=crawler_pb2.Product(product_url=response['product_url'],
                                                                 price=response['price'],
                                                                 title=response['title']))


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=40))
    crawler_pb2_grpc.add_CrawlerServicer_to_server(CrawlerServicer(), server)
    server.add_insecure_port('[::]:50051')
    server.start()
    try:
        while True:
            time.sleep(3600)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':
    serve()
