# -*- encoding:utf-8 -*-
import os


BASE_URL = os.getenv('CRAWLER_BASE_URL', 'https://www.epocacosmeticos.com.br/')
MATCH = os.getenv('CRAWLER_MATCH', '/p')
PRODUCT_TAG_SELECTOR = os.getenv('CRAWLER_PRODUCT_TAG_SELECTOR', 'div')
PRODUCT_CLASS_SELECTOR = os.getenv('CRAWLER_PRODUCT_CLASS_SELECTOR', 'productName')
PRICE_TAG_SELECTOR = os.getenv('CRAWLER_PRICE_TAG_SELECTOR', 'strong')
PRICE_CLASS_SELECTOR = os.getenv('CRAWLER_PRICE_CLASS_SELECTOR', 'skuBestPrice')
NUM_THREADS = os.getenv('CRAWLER_NUM_THREADS', 20)
SERVER = os.getenv('CRAWLER_SERVER', 'localhost:50051')
SITE_NAME = os.getenv('SITE_NAME', 'epocacosmeticos.com.br')
