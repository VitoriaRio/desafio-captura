# -*- encoding:utf-8 -*-
import crawler
from util import write_csv


def run():
    c = crawler.Crawler()
    products = c.do()
    write_csv(products)


if __name__ == '__main__':
    run()
