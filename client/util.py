# -*- encoding:utf-8 -*-
import csv
import config
import datetime


def write_csv(product_list):
    date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    pfile = open(config.SITE_NAME+'-'+date+'.csv', 'w')
    with pfile:
        writer = csv.writer(pfile)
        for product in product_list:
            title = unicode(product['title']).encode("utf-8")
            url = unicode(product['url']).encode("utf-8")
            price = unicode(product['price']).encode("utf-8")
            writer.writerow([url, title, price])
