from mock import MagicMock
from unittest import TestCase
from crawler import Crawler
from proto import crawler_pb2


class ClientTest(TestCase):

    def test_no_products_success(self):
        c = Crawler()
        return_value = crawler_pb2.List(url=['https://epocacosmeticos.com.br/1',
                                             'https://epocacosmeticos.com.br/2',
                                             'https://epocacosmeticos.com.br/3'],
                                        product_info=crawler_pb2.Product(product_url='',
                                                                         price='',
                                                                         title=''))
        c._request = MagicMock(return_value=return_value)
        c._get_stub = MagicMock(return_value='mocked stub')
        c.do()
        self.assertEqual(c.products, [])
        self.assertEqual(len(c.visiteds), 4)
        self.assertEqual(len(c.threads), 0)
        self.assertEqual(len(c.url_queue), 0)

    def test_product_success(self):
                c = Crawler()
                return_value = crawler_pb2.List(url=[],
                                                product_info=crawler_pb2.Product(product_url=c.base_url+'product_page/p',
                                                                                 price='some price',
                                                                                 title='some title'))
                c._request = MagicMock(return_value=return_value)
                c._get_stub = MagicMock(return_value='mocked stub')
                products = c.do()
                self.assertEqual(len(products), 1)
                self.assertEqual(products[0], {'url': (c.base_url+'product_page/p').decode('utf_8'),
                                               'price': 'some price'.decode('utf_8'),
                                               'title': 'some title'.decode('utf_8')})
