# -*- encoding:utf-8 -*-
import grpc
from proto import crawler_pb2_grpc
from proto import crawler_pb2
import threading
import sys
import config


class Crawler(object):
    def __init__(self):
        self.base_url = config.BASE_URL
        self.price_selector = [config.PRICE_TAG_SELECTOR, config.PRICE_CLASS_SELECTOR]
        self.product_selector = [config.PRODUCT_TAG_SELECTOR, config.PRODUCT_CLASS_SELECTOR]
        self.visiteds = []
        self.products = []
        self.url_queue = [self.base_url]
        self.match = config.MATCH
        self.threads = []
        self.num_threads = int(config.NUM_THREADS)
        self.lock = threading.Lock()

    def do(self):
        self._new_thread()
        try:
            while self.threads:
                for t in self.threads:
                    t.join()
                    self.threads.remove(t)
        except KeyboardInterrupt:
            sys.exit()

        return self.products

    def _get_stub(self):
        channel = grpc.insecure_channel(config.SERVER)
        return crawler_pb2_grpc.CrawlerStub(channel)

    def _new_thread(self):
        t = threading.Thread(target=self._map_proccess)
        self.lock.acquire()
        self.threads.append(t)
        self.lock.release()
        t.start()

    def _map_proccess(self):
        '''
            _map_proccess envia as urls para o servidor com
            os seletores e recebe um conjunto de urls e, no
            caso de uma pagina de produto, as informacoes do
            produto.
        '''
        try:
            stub = self._get_stub()
            while self.url_queue:

                self.lock.acquire()
                current_page = self.url_queue.pop(0)
                self.visiteds.append(current_page)
                self.lock.release()

                response = self._request(stub, current_page)
                self.lock.acquire()
                self.url_queue.extend(list((set(response.url) - set(self.visiteds)) - set(self.url_queue)))
                self.lock.release()

                if response.product_info.title != '' and response.product_info.price != '':
                    self.lock.acquire()
                    self.products.append({'title': response.product_info.title,
                                          'url': response.product_info.product_url,
                                          'price': response.product_info.price})
                    self.lock.release()
                if len(self.threads) < self.num_threads:
                    self._new_thread()
        except:
            if self.lock.locked():
                self.lock.release()
            pass

    def _request(self, stub, current_page):
        price_selector = crawler_pb2.Selector(tag=self.price_selector[0],
                                              class_name=self.price_selector[1])
        product_selector = crawler_pb2.Selector(tag=self.product_selector[0],
                                                class_name=self.product_selector[1])
        response = stub.GetUrls(crawler_pb2.Target(current_url=current_page,
                                                   base_url=self.base_url,
                                                   match=self.match,
                                                   price_selector=price_selector,
                                                   title_selector=product_selector), timeout=30)
        return response
